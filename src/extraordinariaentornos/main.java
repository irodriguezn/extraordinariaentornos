/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extraordinariaentornos;

/**
 *
 * @author lliurex
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //double cantidad=1347.73;
        double cantidad=9010.87;
        int [] monedas=cambioMonedas(cantidad);
        cuentaMonedas(monedas);
        muestra(burbuja(new int[]{7,12,5,23,45,15}));
    }
    
    static int[] burbuja(int[] v) {
        int w[]=v.clone();
        int aux;
        int tope=w.length-1;
        while (tope>0) {
            for (int i=0; i<tope; i++) {
                if (w[i]>w[i+1]) {
                    aux=w[i+1];
                    w[i+1]=w[i];
                    w[i]=aux;
                }
            }
            tope--;
        }
        return w;
    }
    
    static void muestra(int[] v) {
        System.out.print("{ ");
        for (int e:v) {
            System.out.print(e+" ");
        }
        System.out.println("}");
    }
    
    public static int[] cambioMonedas(double importe) {
        int [] monedas = new int[8];
        int [] importes={200, 100, 50, 20, 10, 5, 2, 1};
        int indice=0;
        int importeCentimos= (int)(importe*100);
        while (importeCentimos!=0) {
            monedas[indice]=importeCentimos/importes[indice];
            importeCentimos=importeCentimos%importes[indice];
            indice++;
        }
        return monedas;
    }
    
    public static void cuentaMonedas(int[] monedas) {
        if (monedas[0]!=0) {
            System.out.println("2€  : " + monedas[0]);
        }
        if (monedas[1]!=0) {
            System.out.println("1€  : " + monedas[1]);
        }
        if (monedas[2]!=0) {
            System.out.println("50c : " + monedas[2]);
        }
        if (monedas[3]!=0) {
            System.out.println("20c : " + monedas[3]);
        }
        if (monedas[4]!=0) {
            System.out.println("10c : " + monedas[4]);
        }
        if (monedas[5]!=0) {
            System.out.println("5c  : " + monedas[5]);
        }
        if (monedas[6]!=0) {
            System.out.println("2c  : " + monedas[6]);
        }
        if (monedas[7]!=0) {
            System.out.println("1c  : " + monedas[7]);
        }        
    }
}
